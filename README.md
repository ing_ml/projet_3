# Projet_3


Livrables
Un notebook de l'analyse exploratoire mis au propre et annoté.
Un notebook pour chaque prédiction (émissions de CO2 et consommation totale d’énergie) des différents tests de modèles mis au propre, dans lequel vous identifierez clairement le modèle final choisi.
Un support de présentation pour la soutenance (entre 15 et 25 slides).
Pour faciliter votre passage devant le jury, déposez sur la plateforme, dans un dossier zip nommé “Titre_du_projet_nom_prénom”, votre livrable nommé comme suit : Nom_Prénom_n° du livrable_nom du livrable_date de démarrage du projet. Cela donnera :

Nom_Prénom_1_notebook_exploratoire_mmaaaa
Nom_Prénom_2_notebook_prediction_mmaaaa
Nom_Prénom_3_notebook_prediction_mmaaaa
Nom_Prénom_4_presentation_mmaaaa
Par exemple, votre premier livrable peut être nommé comme suit : Dupont_Jean_1_notebook_exploratoire_012022.

Soutenance
La soutenance se déroulera en visioconférence et durera 30 minutes. Le mentor évaluateur ne jouera aucun rôle particulier. Elle s’appuiera sur votre dernier livrable (votre présentation).

Présentation (20 minutes)
Rappel de la problématique et présentation du jeu de données (5 mn).
Présentation du feature engineering (5 minutes).
Explication de l’approche de modélisation et présentation des résultats (10 minutes).
Discussion (5 minutes)
L’évaluateur vous challengera sur vos choix.
Débriefing (5 minutes)
À la fin de la soutenance, vous pourrez débriefer ensemble.
Votre présentation devrait durer 20 minutes (+/- 5 minutes).  Puisque le respect des durées des présentations est important en milieu professionnel, les présentations en dessous de 15 minutes ou au-dessus de 25 minutes peuvent être refusées.

Compétences évaluées
Adapter les hyperparamètres d'un algorithme d'apprentissage supervisé afin de l'améliorer
Transformer les variables pertinentes d'un modèle d'apprentissage supervisé
Mettre en place le modèle d'apprentissage supervisé adapté au problème métier
Évaluer les performances d’un modèle d'apprentissage supervisé



## Contexte


Seattle’s Building Energy Benchmarking and Reporting Program (SMC 22.920) requires owners of non-residential and multifamily buildings (20,000 square feet or larger) to track energy performance and annually report to the City of Seattle. Buildings account for 33% of Seattle's core emissions. The benchmarking policy supports Seattle's goals to reduce energy use and greenhouse gas emissions from existing buildings. In 2013, the City of Seattle adopted a Climate Action Plan to achieve zero net greenhouse gas (GHG) emissions by 2050. Annual benchmarking, reporting and disclosing of building performance are foundational elements of creating more market value for energy efficiency.

Per Ordinance (125000), starting with 2015 energy use performance reporting, the City of Seattle will make the data for all building 20,000 SF and larger available annually. This update to the benchmarking mandate was passed by Seattle City Council on February 29, 2016.

https://www.seattle.gov/environment/climate-change/buildings-and-energy/energy-benchmarking


- **Variables** :
    - OSEBuildingID : A unique identifier assigned to each property covered by the Seattle Benchmarking Ordinance for tracking and identification purposes.
    - DataYear : Calendar year (January-December) represented by each data record.
    - BuildingType : City of Seattle building type classification.
    - PrimaryPropertyType : The primary use of a property (e.g. office, retail store). Primary use is defined as a function that accounts for more than 50% of a property. This is the Property Type - EPA Calculated field from Portfolio Manager.
    - PropertyName : Official or common property name as entered in EPA’s Portfolio Manager.
    - Address : Property street address
    - City : Property city
    - State : Property state
    - ZipCode : Property zip
    - TaxParcelIdentificationNumber : Property King County PIN
    - CouncilDistrictCode : Property City of Seattle council district.
    - Neighborhood : Property neighborhood area defined by the City of Seattle Department of Neighborhoods.
    - Latitude : Property latitude.
    - Longitude : Property Longitude.
    - YearBuilt : Year in which a property was constructed or underwent a complete renovation.
    - NumberofBuildings : Number of buildings included in the property's report. In cases where a property is reporting as a campus, multiple buildings may be included in one report.
    - NumberofFloors : Number of floors reported in Portfolio Manager
    - PropertyGFATotal : Total building and parking gross floor area.
    - PropertyGFAParking : Total space in square feet of all types of parking (Fully Enclosed, Partially Enclosed, and Open).
    - PropertyGFABuilding(s) : Total floor space in square feet between the outside surfaces of a building’s enclosing walls. This includes all areas inside the building(s), such as tenant space, common areas, stairwells, basements, storage, etc.
    - ListOfAllPropertyUseTypes : All property uses reported in Portfolio Manager
    - LargestPropertyUseType : The largest use of a property (e.g. office, retail store) by GFA.
    - LargestPropertyUseTypeGFA : The gross floor area (GFA) of the largest use of the property.
    - SecondLargestPropertyUseType : The second largest use of a property (e.g. office, retail store) by GFA.
    - SecondLargestPropertyUseTypeGFA : The gross floor area (GFA) of the second largest use of the property.
    - ThirdLargestPropertyUseType : The third largest use of a property (e.g. office, retail store) by GFA.
    - ThirdLargestPropertyUseTypeGFA : The gross floor area (GFA) of the third largest use of the property.
    - YearsENERGYSTARCertified : Years the property has received ENERGY STAR certification.
    - ENERGYSTARScore : An EPA calculated 1-100 rating that assesses a property’s overall energy performance, based on national data to control for differences among climate, building uses, and operations. A score of 50 represents the national median.
    - SiteEUI(kBtu/sf) : Site Energy Use Intensity (EUI) is a property's Site Energy Use divided by its gross floor area. Site Energy Use is the annual amount of all the energy consumed by the property on-site, as reported on utility bills. Site EUI is measured in thousands of British thermal units (kBtu) per square foot.
    - SiteEUIWN(kBtu/sf) : Weather Normalized (WN) Site Energy Use Intensity (EUI) is a property's WN Site Energy divided by its gross floor area (in square feet). WN Site Energy is the Site Energy Use the property would have consumed during 30-year average weather conditions. WN Site EUI is measured in measured in thousands of British thermal units (kBtu) per square foot.
    - SourceEUI(kBtu/sf) : Source Energy Use Intensity (EUI) is a property's Source Energy Use divided by its gross floor area. Source Energy Use is the annual energy used to operate the property, including losses from generation, transmission, & distribution. Source EUI is measured in thousands of British thermal units (kBtu) per square foot.
    - SourceEUIWN(kBtu/sf) : Weather Normalized (WN) Source Energy Use Intensity (EUI) is a property's WN Source Energy divided by its gross floor area. WN Source Energy is the Source Energy Use the property would have consumed during 30-year average weather conditions. WN Source EUI is measured in measured in thousands of British thermal units (kBtu) per square foot.
    - SiteEnergyUse(kBtu) : The annual amount of energy consumed by the property from all sources of energy.
    - SiteEnergyUseWN(kBtu) : The annual amount of energy consumed by the property from all sources of energy, adjusted to what the property would have consumed during 30-year average weather conditions.
    - SteamUse(kBtu) : The annual amount of district steam consumed by the property on-site, measured in thousands of British thermal units (kBtu).
    - Electricity(kWh) : The annual amount of electricity consumed by the property on-site, including electricity purchased from the grid and generated by onsite renewable systems, measured in kWh.
    - Electricity(kBtu) : The annual amount of electricity consumed by the property on-site, including electricity purchased from the grid and generated by onsite renewable systems, measured in thousands of British thermal units (kBtu).
    - NaturalGas(therms) : The annual amount of utility-supplied natural gas consumed by the property, measured in therms.
    - NaturalGas(kBtu) : The annual amount of utility-supplied natural gas consumed by the property, measured in thousands of British thermal units (kBtu).
    - DefaultData : The property used default data for at least one property characteristic.
    - Comments : Comments by a building owner or agent to provide context to the building’s energy use.
    - ComplianceStatus : Whether a property has met energy benchmarking requirements for the current reporting year.
    - Outlier : Whether a property is a high or low outlier (Y/N)
    - TotalGHGEmissions : The total amount of greenhouse gas emissions, including carbon dioxide, methane, and nitrous oxide gases released into the atmosphere as a result of energy consumption at the property, measured in metric tons of carbon dioxide equivalent. This calculation uses a GHG emissions factor from Seattle CIty Light's portfolio of generating resources. This uses Seattle City Light's 2015 emissions factor of 52.44 lbs CO2e/MWh until the 2016 factor is available. Enwave steam factor = 170.17 lbs CO2e/MMBtu. Gas factor sourced from EPA Portfolio Manager = 53.11 kg CO2e/MBtu.
    - GHGEmissionsIntensity : Total Greenhouse Gas Emissions divided by property's gross floor area, measured in kilograms of carbon dioxide equivalent per square foot. This calculation uses a GHG emissions factor from Seattle City Light's portfolio of generating resources
   



Lien utiles:

- [Les métriques energy score](https://www.energystar.gov/buildings/benchmark/understand_metrics/how_score_calculated)
- [Les métriques energy eui](https://www.energystar.gov/buildings/benchmark/understand_metrics/what_eui)
- [Les métriques energy dif source/site](https://www.energystar.gov/buildings/benchmark/understand_metrics/source_site_difference)
- [Aide si coincé](https://www.kaggle.com/code/michaelfumery/seattle-building-energy-cleaning/notebook)

