
def evaluation(model, metric_name=False):
    if metric_name:
        import sklearn
        print(sklearn.metrics.get_scorer_names())
    # model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    print(model)
    print(model.score(X_test, y_test))
    mae = mean_absolute_error(y_true=y_test,y_pred=y_pred)
    #squared True returns MSE value, False returns RMSE value.
    mse = mean_squared_error(y_true=y_test,y_pred=y_pred) #default=True
    rmse = mean_squared_error(y_true=y_test,y_pred=y_pred,squared=False)

    print("MAE:",mae)
    print("MSE:",mse)
    print("RMSE:",rmse)
    errors = abs(y_pred - y_test)
    # Mean Absolute Error (MAE)
    print('MAE =', round(np.mean(errors), 2), 'unit')

    # mean absolute percentage error (MAPE)
    mape = 100 * (errors / y_test)
    # Précision
    prec = 100 - np.mean(mape)
    print('Précision :', round(prec, 2), '%.')

    score_train = model.score(X_train,y_train)
    score_test = model.score(X_test,y_test)
    print("score_train =",round(score_train,3),"| score_test =", round(score_test,3))
    visualizer = ResidualsPlot(model)
    visualizer.fit(X_train, y_train)
    visualizer.score(X_test, y_test)
    visualizer.poof()
    print('-'*100)


def model_scores(pip, step):
    df_results = pd.DataFrame.from_dict(pip.named_steps[step].cv_results_) \
        .sort_values('rank_test_neg_mean_absolute_error')
    best_nmae = pip.named_steps[step].best_score_
    best_r2 = np.mean(df_results[df_results.rank_test_r2 == 1]['mean_test_r2'])
    best_params = pip.named_steps[step].best_params_
    training_time = round((np.mean(df_results.mean_fit_time)*X_train.shape[0]),2)
    print(f"Meilleur score MAE : {round(best_nmae,3)}\nMeilleur Score R2 : {round(best_r2,3)}\nMeilleurs paramètres : {best_params}\nTemps moyen d'entrainement : {training_time}s")
    d = {'model': step, 'MAE': best_nmae, 'R2': best_r2, 'Best_params': best_params, 'training_time': training_time}
    model_selection_df = pd.DataFrame(d)
    pd.to_csv('/model_selection/'+step+'.csv')
    return df_results






def transformers_encoders(df, target : str):

    categorical_features = df.select_dtypes(include=['object']).columns
    numeric_features = df.drop(target, axis=1).select_dtypes(exclude=['object']).columns

    numeric_transformers = [StandardScaler(), RobustScaler()]
    categorical_transformers = [OneHotEncoder(handle_unknown="ignore", sparse=False), TargetEncoder()]

    preprocessor = ColumnTransformer(transformers=[
        ('cat',  categorical_transformers[1], categorical_features),
        ('num', numeric_transformers[1], numeric_features)
    ])
    return preprocessor

models = [
        [LinearRegression(),  {"fit_intercept": [True, False],
                                           "normalize": [True, False]}],
        [RandomForestRegressor(), {#'max_features' : ['sqrt', 'log2'],
    # "criterion": ["squared_error", "mae"],
    'max_depth': [5, 15, 25, 50],
    'min_samples_split': [2, 5, 10],
    # 'bootstrap' : [True, False],
    'min_samples_leaf': [1,2,5,10]}],
        # Ridge(),
        # ElasticNet(),
        # Lasso()
    ]

def global_gridsearch(model_params):

    grid_cv_model = Pipeline([
        ('preprocessor', preprocessor),
        (model_params[0].__class__.__name__, GridSearchCV(model_params[0],
                                         param_grid=model_params[1],
                                         cv=5,
                                         scoring=('r2','neg_mean_absolute_error'),
                                         return_train_score = True,
                                         refit='neg_mean_absolute_error',
                                         n_jobs = -1))])
    return grid_cv_model

def plot_search_results(grid):
    """
    Params:
        grid: A trained GridSearchCV object.
    """
    ## Results from grid search
    results = grid.cv_results_
    means_test = results['mean_test_score']
    stds_test = results['std_test_score']
    # means_train = results['mean_train_score']
    # stds_train = results['std_train_score']

    ## Getting indexes of values per hyper-parameter
    masks=[]
    masks_names= list(grid.best_params_.keys())
    for p_k, p_v in grid.best_params_.items():
        masks.append(list(results['param_'+p_k].data==p_v))

    params=grid.param_grid

    ## Ploting results
    fig, ax = plt.subplots(1,len(params),sharex='none', sharey='all',figsize=(20,5))
    fig.suptitle('Score per parameter')
    fig.text(0.04, 0.5, 'MEAN SCORE', va='center', rotation='vertical')
    pram_preformace_in_best = {}
    for i, p in enumerate(masks_names):
        m = np.stack(masks[:i] + masks[i+1:])
        pram_preformace_in_best
        best_parms_mask = m.all(axis=0)
        best_index = np.where(best_parms_mask)[0]
        x = np.array(params[p])
        y_1 = np.array(means_test[best_index])
        e_1 = np.array(stds_test[best_index])
        # y_2 = np.array(means_train[best_index])
        # e_2 = np.array(stds_train[best_index])
        ax[i].errorbar(x, y_1, e_1, linestyle='--', marker='o', label='test')
        # ax[i].errorbar(x, y_2, e_2, linestyle='-', marker='^',label='train' )
        ax[i].set_xlabel(p.upper())

    plt.legend()
    plt.show()